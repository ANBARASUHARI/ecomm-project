from django import forms

class PaymentForm(forms.Form):
    payment_method = forms.ChoiceField(choices=[('paypal', 'PayPal'), ('upi', 'UPI'), ('card', 'Credit/Debit Card'), ('cod', 'Cash on Delivery')], widget=forms.RadioSelect)