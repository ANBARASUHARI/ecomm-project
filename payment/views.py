from django.shortcuts import render, get_object_or_404
from .models import Order, PaymentDetail
from .forms import PaymentForm

# Create your views here.

def payment_page(request, order_id):
    order = get_object_or_404(Order, pk=order_id)
    form = PaymentForm()

    if request.method == 'POST':
        form = PaymentForm(request.POST)
        if form.is_valid():
            payment_method = form.cleaned_data['payment_method']
            payment_detail = PaymentDetail(order=order, payment_method=payment_method)
            payment_detail.save()
            order.payment = payment_detail
            order.save()

            return render(request, 'payment/success.html', {'order': order, 'payment_detail': payment_detail})

    return render(request, 'payment/payment.html', {'order': order, 'form': form})
